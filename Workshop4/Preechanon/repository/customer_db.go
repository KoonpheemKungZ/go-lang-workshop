package repository

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
)

type customerRepositoryDB struct {
	db *sqlx.DB
}

func NewCustomerRepositoryDB(db *sqlx.DB) customerRepositoryDB {
	return customerRepositoryDB{db: db}
}

func (r customerRepositoryDB) GetAll() ([]Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "SELECT * FROM customer"
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("DEBUG: executing query: %s\n", query)
	rows, err := r.db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("Error executing query: %v\n", err)
	}
	defer rows.Close()
	customer := make([]Customer, 0)
	for rows.Next() {
		var c Customer
		rows.Scan(&c.Id, &c.Name, &c.Phone_number, &c.Date_created)
		log.Printf("DEBUG: retrieved customer with id: %d, name: %s, phone_number: %s, date_created: %s\n",
			c.Id,
			c.Name,
			c.Phone_number,
			c.Date_created,
		)
		customer = append(customer, c)
	}
	return customer, nil
}

func (r customerRepositoryDB) GetById(id int) (*Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "SELECT * FROM customer WHERE id = ?"
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("DEBUG: executing query: %s with id: %d\n", query, id)
	rows := r.db.QueryRowContext(ctx, query, id)
	customer := &Customer{}
	err := rows.Scan(&customer.Id, &customer.Name, &customer.Phone_number, &customer.Date_created)
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("No customer found with id: %d\n", id)
	} else if err != nil {
		return nil, fmt.Errorf("Error executing query: %v\n", err)
	}
	log.Printf("DEBUG: retrieved customer with id: %d, name: %s, phone_number: %s, date_created: %s\n",
		customer.Id,
		customer.Name,
		customer.Phone_number,
		customer.Date_created,
	)
	return customer, nil
}

func (r customerRepositoryDB) DeleteCustomer(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "DELETE FROM customer WHERE id = ?"
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("DEBUG: executing query: %s with id: %d\n", query, id)
	_, err := r.db.ExecContext(ctx, query, id)
	if err != nil {
		return fmt.Errorf("Error executing query: %v\n", err)
	}
	log.Printf("DEBUG: successfully deleted record with id: %d\n", id)
	return nil
}

func (r customerRepositoryDB) AddCustomer(c Customer) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := `
        INSERT INTO customer (id, name, phone_number, date_created) VALUES (?, ?, ?, ?)
    `
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Printf("DEBUG: executing query: %s with params: %v\n", query, []interface{}{c.Id, c.Name, c.Phone_number, c.Date_created})
	rows, err := r.db.ExecContext(ctx, query, c.Id, c.Name, c.Phone_number, c.Date_created)
	if err != nil {
		return 0, fmt.Errorf("Error executing query: %v\n", err)
	}
	insertID, err := rows.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("Error getting last insert id: %v\n", err)
	}
	log.Printf("DEBUG: inserted record with id: %v\n", insertID)
	return int(insertID), nil
}

func (r customerRepositoryDB) UpdateCustomer(c Customer, id int) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	rows, err := r.db.ExecContext(ctx, "UPDATE Customer SET name = ?, phone_number = ?, date_created = ? WHERE id = ?",
		c.Name,
		c.Phone_number,
		c.Date_created,
		id,
	)
	if err != nil {
		if strings.Contains(err.Error(), "no rows") {
			log.Println("DEBUG: customer id not found in the table")
		}
		return 0, err
	}

	rowsAffected, err := rows.RowsAffected()
	if err != nil {
		return 0, err
	}

	if rowsAffected == 0 {
		return 0, fmt.Errorf("customer with id %d not found", id)
	}

	updateID, err := rows.LastInsertId()
	if err != nil {
		return 0, err
	}
	log.Printf("DEBUG: customer with id %d updated successfully\n", id)
	return int(updateID), nil
}
