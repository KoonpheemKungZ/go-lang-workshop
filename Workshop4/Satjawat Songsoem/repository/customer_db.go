package repository

import (
	"log"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type customerRepositoryDB struct {
	db *sqlx.DB
}

func NewCustomerRepositoryDB(db *sqlx.DB) customerRepositoryDB {
	return customerRepositoryDB{db: db}
}

func (r customerRepositoryDB) InsertCustomer(customer Customer) (int, error) {
	query := "INSERT INTO customers (Customer_id, Customer_name, Phone_number, Date_created) VALUES (?, ?, ?,?)"
	result, err := r.db.Exec(query, customer.CustomerID, customer.CustomerName, customer.PhoneNumber, customer.DateCreated)
	if err != nil {
		log.Printf("Error inserting customer: %v", err)
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		log.Printf("Error getting last insert ID: %v", err)
		return 0, err
	}
	log.Printf("Inserted customer with ID %d", lastInsertID)
	return int(lastInsertID), nil
}

func (r customerRepositoryDB) RemoveCustomer(customerID int) error {
	query := "DELETE FROM customers WHERE Customer_id = ?"
	_, err := r.db.Exec(query, customerID)
	if err != nil {
		log.Printf("Error removing customer with ID %d: %v", customerID, err)
		return err
	}
	log.Printf("Removed customer with ID %d", customerID)
	return nil
}

func (r customerRepositoryDB) UpdateCustomer(customer Customer) (int, error) {
	query := "UPDATE Customers SET Customer_name = ?, Phone_number = ?, Date_created = ? WHERE Customer_id = ?"
	result, err := r.db.Exec(query, customer.CustomerName, customer.PhoneNumber, customer.DateCreated, customer.CustomerID)
	if err != nil {
		log.Printf("Error updating customer with ID %d: %v", customer.CustomerID, err)
		return 0, err
	}
	updateID, err := result.LastInsertId()
	if err != nil {
		log.Printf("Error getting last insert ID: %v", err)
		return 0, err
	}
	log.Printf("Updated customer with ID %d", customer.CustomerID)
	return int(updateID), nil
}

func (r customerRepositoryDB) GetAll() ([]Customer, error) {
	customers := []Customer{}
	query := "SELECT Customer_id, Customer_name, Phone_number, Date_created FROM customers"
	err := r.db.Select(&customers, query)
	if err != nil {
		log.Printf("Error getting all customers: %v", err)
		return nil, err
	}
	log.Printf("Retrieved all customers (%d total)", len(customers))
	return customers, nil
}

func (r customerRepositoryDB) GetById(id int) (*Customer, error) {
	customer := Customer{}
	query := "SELECT Customer_id, Customer_name, Phone_number, Date_created FROM customers WHERE Customer_id = ?"
	err := r.db.Get(&customer, query, id)
	if err != nil {
		log.Printf("Error getting customer with ID %d: %v", id, err)
		return nil, err
	}
	log.Printf("Retrieved customer with ID %d", id)
	return &customer, nil
}
