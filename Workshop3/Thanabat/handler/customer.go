package handler

import (
	"Thanabat/Workshop3/Thanabat/service"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type customerHandler struct {
	customerService service.CustomerService
}

func NewCustomerHandler(customerService service.CustomerService) customerHandler {
	return customerHandler{customerService: customerService}
}

func (h customerHandler) GetCustomers(w http.ResponseWriter, r *http.Request) {
	customers, err := h.customerService.GetCustomers()
	if err != nil {
		handleError(w, err)
		return
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(customers)
}
func (h customerHandler) GetCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["CustomerID"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	customer, err := h.customerService.GetCustomer(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	json.NewEncoder(w).Encode(customer)
}

func (h customerHandler) DeleteCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	customerID, err := strconv.Atoi(vars["CustomerID"])
	if err != nil {
		handleError(w, err)
		return
	}

	err = h.customerService.DeleteCustomer(customerID)
	if err != nil {
		handleError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
func (h customerHandler) InsertCustomer(w http.ResponseWriter, r *http.Request) {
	request := service.CustomerRequest{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, err)
		return
	}

	response, err := h.customerService.InsertCustomer(request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("content-type", "applicaiton/json")
	json.NewEncoder(w).Encode(response)
}
func (h customerHandler) UpdateCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["CustomerID"])
	if err != nil {
		handleError(w, err)
		return
	}

	request := service.CustomerRequest{}
	err = json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		handleError(w, err)
		return
	}

	response, err := h.customerService.UpdateCustomer(id, request)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}
